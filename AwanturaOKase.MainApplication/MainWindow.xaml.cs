﻿using System;
using System.Windows;
using AwanturaOKase.MainApplication.Game;
using AwanturaOKase.MainApplication.Panel;

namespace AwanturaOKase.MainApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private AdminGameWindow _mgw;
        public MainWindow()
        {
            InitializeComponent();
            Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
        }

        private void Current_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show($"Wystąpił błąd. Komunikat poniżej: \n\n\t{e.Exception.Message}");
            MessageBox.Show(
                "W większości wypadków pomaga pobranie MsSQL LocalDb 2012, do pobrania w linku: http://goo.gl/OXxNqJ");
            e.Handled = true;
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Application.Current.Shutdown();
        }

        private void teamSettings_Click(object sender, RoutedEventArgs e)
        {
            _mainFrame.Navigate(new TeamsEditor());
        }

        private void questionSettings_Click(object sender, RoutedEventArgs e)
        {
            _mainFrame.Navigate(new QuestionEditor());
        }

        private void gameSettings_Click(object sender, RoutedEventArgs e)
        {
            _mainFrame.Navigate(new GameSettings());
        }

        private void gameStart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_mgw == null)
                    _mgw = new AdminGameWindow();
                _mgw.Show();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Okno pojawi się za chwilę.");
                if (ex.GetType() != typeof(NullReferenceException))
                {
                    _mgw?.Show();
                }
            }
        }
    }
}
