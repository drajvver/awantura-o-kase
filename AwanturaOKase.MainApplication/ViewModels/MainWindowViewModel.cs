﻿using System.ComponentModel;

namespace AwanturaOKase.MainApplication.ViewModels
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        AwanturaDbEntities ctx = new AwanturaDbEntities();
        public MainWindowViewModel()
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string Obj)
        {
            if (PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(Obj));
            }
        }
    }
}
