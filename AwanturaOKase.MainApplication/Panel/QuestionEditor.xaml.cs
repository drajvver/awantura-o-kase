﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AwanturaOKase.MainApplication.Panel.PanelViewModels;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Microsoft.Win32;

namespace AwanturaOKase.MainApplication.Panel
{
    /// <summary>
    /// Interaction logic for QuestionEditor.xaml
    /// </summary>
    public partial class QuestionEditor : Page
    {
        private readonly QuestionEditorViewModel _qevm = new QuestionEditorViewModel();

        public QuestionEditor()
        {
            InitializeComponent();
            DataContext = _qevm;
        }

        private void addQuestion_Click(object sender, RoutedEventArgs e)
        {
            bool isChecked = false;
            Questions q = new Questions();
            q.QuestionContent = questionText.Text;
            var questionTypes = categoryCombo.SelectedItem as QuestionTypes;
            if (questionTypes != null)
                q.QuestionType = questionTypes.QuestionTypeId;
            else
            {
                MessageBox.Show("Wybierz kategorię!");
                return;
            }
            q.QuestionTypes = (categoryCombo.SelectedItem as QuestionTypes);
            q.QuestionAnswerA = Answer1.Text;
            q.QuestionAnswerB = Answer2.Text;
            q.QuestionAnswerC = Answer3.Text;
            q.QuestionAnswerD = Answer4.Text;

            foreach(RadioButton rb in hintsGrid.Children.OfType<RadioButton>())
            {
                if(rb.IsChecked == true)
                {
                    switch(rb.Name)
                    {
                        case "isRightAnswer1":
                            q.QuestionRightAnswer = Answer1.Text;
                            isChecked = true;
                            break;
                        case "isRightAnswer2":
                            q.QuestionRightAnswer = Answer2.Text;
                            isChecked = true;
                            break;
                        case "isRightAnswer3":
                            q.QuestionRightAnswer = Answer3.Text;
                            isChecked = true;
                            break;
                        case "isRightAnswer4":
                            q.QuestionRightAnswer = Answer4.Text;
                            isChecked = true;
                            break;
                    }
                }
            }

            if (!isChecked)
            {
                MessageBox.Show("Musisz wybrać i wpisać przynajmniej jedną odpowiedź, która będzie poprawna!");
                return;
            }

            _qevm.UpdateOrAddNewQuestion(q);
            questionText.ItemsSource = _qevm.QList.Where(t => t.QuestionType == q.QuestionType);
            questionText.Items.Refresh();
            MessageBox.Show("Dodano pytanie.");
        }

        private void categoryCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            QuestionTypes qt = (categoryCombo.SelectedItem as QuestionTypes);
            if(qt != null)
            {
                questionText.Text = "";
                questionText.ItemsSource = _qevm.QList.Where(x => x.QuestionType == qt.QuestionTypeId).ToList();
                CategoryTypeImage.Source = qt.QuestionImgImageSource ??
                                           new BitmapImage(new Uri("../Images/Placeholder.jpg", UriKind.RelativeOrAbsolute));
            }
        }

        private void questionText_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Questions q = (sender as ComboBox).SelectedItem as Questions;
            foreach (RadioButton rb in hintsGrid.Children.OfType<RadioButton>())
            {
                rb.IsChecked = false;
            }
            if (q != null)
            {
                Answer1.Text = q.QuestionAnswerA ?? "";
                Answer2.Text = q.QuestionAnswerB ?? "";
                Answer3.Text = q.QuestionAnswerC ?? "";
                Answer4.Text = q.QuestionAnswerD ?? "";
                if (q.QuestionRightAnswer != null)
                {
                    if (q.QuestionRightAnswer == q.QuestionAnswerA)
                    {
                        isRightAnswer1.IsChecked = true;
                    }
                    else if (q.QuestionRightAnswer == q.QuestionAnswerB)
                    {
                        isRightAnswer2.IsChecked = true;
                    }
                    else if (q.QuestionRightAnswer == q.QuestionAnswerC)
                    {
                        isRightAnswer3.IsChecked = true;
                    }
                    else if (q.QuestionRightAnswer == q.QuestionAnswerD)
                    {
                        isRightAnswer4.IsChecked = true;
                    }
                }
            }
        }

        private void SaveCategoryButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (CategoryNameBox.Text != "")
            {
                _qevm.UpdateOrAddCategory(CategoryNameBox.Text, CategoryTypeImage.Source, DrawOnlyOnceCheckBox.IsChecked);
                MessageBox.Show("Dodano kategorię.");
            }
            else
            {
                MessageBox.Show("Błąd, podaj nazwę kategorii.");
            }
        }

        private void CategoryTypeImage_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Image Files (*.png, *.jpg)|*.png;*.jpg";
            if (ofd.ShowDialog() == true)
            {
                BitmapImage bi = new BitmapImage(new Uri(ofd.FileName));
                CategoryTypeImage.Source = bi;
            }
        }

        private void AddImageQuestionButton_OnClick(object sender, RoutedEventArgs e)
        {


            if (categoryCombo.Text != "")
            {
                ImageQuestionAdd iqa = new ImageQuestionAdd {q = categoryCombo.Text};
                bool? DialRes = iqa.ShowDialog();
                if (DialRes == true)
                {
                    MessageBox.Show("Dodano pytanie.");
                }
                else if (DialRes == false)
                {
                    MessageBox.Show("Nie dodano pytania.");
                }
                else
                {
                    MessageBox.Show("Błąd podczas dodawania pytania, spróbuj ponownie");
                }
            }
            else
            {
                MessageBox.Show("Najpierw wybierz kategorię!");
            }

        }

        private void DeleteCategory_OnClick(object sender, RoutedEventArgs e)
        {
            _qevm.RemoveCategory(categoryCombo.SelectedItem as QuestionTypes);
            _qevm.QTypes.Remove(categoryCombo.SelectedItem as QuestionTypes);

            MessageBox.Show($"Usunięto kategorię {(categoryCombo.SelectedItem as QuestionTypes)?.QuestionTypeName}");
            //categoryCombo.Items.RemoveAt(categoryCombo.SelectedIndex);
            categoryCombo.SelectedIndex = 0;
        }

        private void DeleteQuestionsButton_OnClick(object sender, RoutedEventArgs e)
        {
            ViewAndDeleteQuestionsWindow vadqwWindow = new ViewAndDeleteQuestionsWindow();
            vadqwWindow.ShowDialog();
        }

        private void Answer1_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if ((sender as TextBox).Text.Length > 32)
            {
                e.Handled = false;
                MessageBox.Show("Maksymalna długość podpowiedzi to 32 znaki!");
            }
        }
    }
}
