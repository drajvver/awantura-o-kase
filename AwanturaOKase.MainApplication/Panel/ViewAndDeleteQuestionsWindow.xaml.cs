﻿using System.Linq;
using System.Windows;

namespace AwanturaOKase.MainApplication.Panel
{
    /// <summary>
    /// Interaction logic for ViewAndDeleteQuestionsWindow.xaml
    /// </summary>
    public partial class ViewAndDeleteQuestionsWindow : Window
    {

        private AwanturaDbEntities ctx = new AwanturaDbEntities();

        public ViewAndDeleteQuestionsWindow()
        {
            InitializeComponent();
        }

        private void removeQuestion_Click(object sender, RoutedEventArgs e)
        {
            var item = AllQuestionsListBox.SelectedItem;
            if(AllQuestionsListBox.SelectedItem != null)
            {
                ctx.Questions.Remove(item as Questions);
                ctx.SaveChanges();
                AllQuestionsListBox.ItemsSource = null;
                AllQuestionsListBox.ItemsSource = ctx.Questions.ToList();
            }
            else
            {
                MessageBox.Show("Wybierz lewym przyciskiem myszy pytanie!");
            }
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            ctx.SaveChanges();
            MessageBox.Show("Pomyślnie usunięto pytania.");
            Close();
        }

        private void ViewAndDeleteQuestionsWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            AllQuestionsListBox.ItemsSource = ctx.Questions.ToList();
        }
    }
}
