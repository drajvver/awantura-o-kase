﻿using System.Data.Entity;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace AwanturaOKase.MainApplication.Panel
{
    /// <summary>
    /// Interaction logic for GameSettings.xaml
    /// </summary>
    public partial class GameSettings : Page
    {
        private readonly AwanturaDbEntities ctx = new AwanturaDbEntities();

        public GameSettings()
        {
            InitializeComponent();
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            SetProp(StartingMoneyTB, "StartingMoney");
            SetProp(RoundTimeTB, "RoundTime");
            ctx.SaveChanges();
        }

        private void SetProp(TextBox tb, string Key)
        {
            Config singleConfig = null;
            singleConfig = (from s in ctx.Configs where s.ConfigKey == Key select s).FirstOrDefault();
            if (singleConfig == null)
            {
                if (tb.Text != "")
                {
                    singleConfig = new Config();
                    singleConfig.ConfigKey = Key;
                    singleConfig.ConfigValue = tb.Text;
                    ctx.Configs.Add(singleConfig);
                }
            }
            else
            {
                if (tb.Text != singleConfig.ConfigValue)
                {
                    singleConfig.ConfigValue = tb.Text;
                    ctx.Entry(singleConfig).State = EntityState.Modified;
                }
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            StartingMoneyTB.Text = ConfigTools.GetConfig(ctx, "StartingMoney");
            RoundTimeTB.Text = ConfigTools.GetConfig(ctx, "RoundTime");
        }
    }
}
