﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Microsoft.Win32;

namespace AwanturaOKase.MainApplication.Panel
{
    /// <summary>
    /// Interaction logic for ImageQuestionAdd.xaml
    /// </summary>
    public partial class ImageQuestionAdd : Window
    {
        public string q = "";
        private AwanturaDbEntities ctx = new AwanturaDbEntities();

        public ImageQuestionAdd()
        {
            InitializeComponent();
        }

        private void QuestionImage_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Image Files (*.png, *.jpg)|*.png;*.jpg";
            if (ofd.ShowDialog() == true)
            {
                BitmapImage bi = new BitmapImage(new Uri(ofd.FileName));
                QuestionImage.Source = bi;
            }
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                QuestionTypes qt = ctx.QuestionTypes.FirstOrDefault(x => x.QuestionTypeName.Equals(q));
                Questions qs = new Questions();

                qs.QuestionContent = QuestionContentBox.Text;
                qs.QuestionType = qt.QuestionTypeId;
                qs.QuestionAnswerA = QuestionRightAnswerBox.Text;
                qs.QuestionTypes = qt;
                qs.QuestionImgImageSource = QuestionImage.Source;
                qs.QuestionRightAnswer = QuestionRightAnswerBox.Text;

                ctx.Questions.Add(qs);
                ctx.SaveChanges();

                DialogResult = true;
                Close();
            }
            catch (Exception exception)
            {
                DialogResult = false;
                Close();
            }
        }
    }
}
