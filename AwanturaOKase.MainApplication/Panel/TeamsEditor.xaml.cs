﻿using System.Windows;
using System.Windows.Controls;
using AwanturaOKase.MainApplication.Panel.PanelViewModels;

namespace AwanturaOKase.MainApplication.Panel
{
    /// <summary>
    /// Interaction logic for TeamsEditor.xaml
    /// </summary>
    public partial class TeamsEditor : Page
    {
        readonly TeamsEditorViewModel TEVM = new TeamsEditorViewModel();
        public TeamsEditor()
        {
            InitializeComponent();
            DataContext = TEVM;
        }

        private void saveTeamBtn_Click(object sender, RoutedEventArgs e)
        {
            Teams t = new Teams();
            t.TeamName = teamNameBox.Text;
            TEVM.UpdateOrAddNew(t);
            DataContext = null;
            DataContext = TEVM;
        }

        private void removeTeam_Click(object sender, RoutedEventArgs e)
        {
            TEVM.RemoveTeam(teamsListBox.SelectedItem as Teams);
            TEVM.Teams.Remove(teamsListBox.SelectedItem as Teams);
            //Teams t = (sender as ListBox).SelectedItem as Teams;
            //TEVM.RemoveTeam(t);
        }
    }
}
