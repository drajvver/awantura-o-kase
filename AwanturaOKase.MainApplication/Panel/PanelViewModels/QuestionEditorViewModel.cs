﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Media;
using AwanturaOKase.MainApplication.Annotations;

namespace AwanturaOKase.MainApplication.Panel.PanelViewModels
{
    class QuestionEditorViewModel : INotifyPropertyChanged
    {
        private readonly AwanturaDbEntities _ctx = new AwanturaDbEntities();

        public QuestionEditorViewModel()
        {
            QTypes = new ObservableCollection<QuestionTypes>(_ctx.QuestionTypes.ToList());
            QList = new ObservableCollection<Questions>(_ctx.Questions.ToList());
        }

        public ObservableCollection<QuestionTypes> QTypes { get; set; }

        public ObservableCollection<Questions> QList { get; set; }

        

        public void UpdateOrAddNewQuestion(Questions q)
        {
            var qq = _ctx.Questions.FirstOrDefault(x => x.QuestionContent == q.QuestionContent);
            if(qq == null)
            {
                _ctx.Questions.Add(q);
            }
            else
            {
                qq.QuestionAnswerA = q.QuestionAnswerA;
                qq.QuestionAnswerB = q.QuestionAnswerB;
                qq.QuestionAnswerC = q.QuestionAnswerC;
                qq.QuestionAnswerD = q.QuestionAnswerD;
                qq.QuestionContent = q.QuestionContent;
                qq.QuestionRightAnswer = q.QuestionRightAnswer;
                _ctx.Entry(qq).State = EntityState.Modified;
            }
            _ctx.SaveChanges();

            OnPropertyChanged(nameof(QList));
        }

        public void UpdateOrAddCategory(string catName, ImageSource catImageSource, bool? isChecked)
        {
            var q = _ctx.QuestionTypes.FirstOrDefault(x => x.QuestionTypeName == catName);
            if (q == null)
            {
                q = new QuestionTypes
                {
                    QuestionTypeName = catName,
                    QuestionImgImageSource = catImageSource,
                    QuestionDrawOnce = isChecked
                };
                _ctx.QuestionTypes.Add(q);
                QTypes.Add(q);
            }
            _ctx.SaveChanges();
        }

        public void RemoveCategory(QuestionTypes qt)
        {
            if (qt != null)
            {
                _ctx.Entry(qt).State = EntityState.Deleted;
                _ctx.SaveChanges();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
