﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;

namespace AwanturaOKase.MainApplication.Panel.PanelViewModels
{
    class TeamsEditorViewModel : INotifyPropertyChanged
    {
        private readonly AwanturaDbEntities _ctx = new AwanturaDbEntities();

        public TeamsEditorViewModel()
        {
            Teams = new ObservableCollection<Teams>(_ctx.Teams.ToList());
        }

        public ObservableCollection<Teams> Teams { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string Obj)
        {
            if (PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(Obj));
            }
        }

        public void UpdateOrAddNew(Teams t)
        {
            var team = _ctx.Teams.FirstOrDefault(x => x.TeamName == t.TeamName);
            if(team == null)
            {
                team = new Teams {TeamName = t.TeamName};
                _ctx.Teams.Add(team);
                _ctx.SaveChanges();
            }
            else
            {
                team.TeamName = t.TeamName;
                _ctx.Entry(team).State = EntityState.Modified;
                _ctx.SaveChanges();
            }
            Teams = new ObservableCollection<Teams>(_ctx.Teams.ToList());
        }

        public void RemoveTeam(Teams t)
        {
            if (t != null)
            {
                _ctx.Entry(t).State = EntityState.Deleted;
                _ctx.SaveChanges();
            }
        }
    }
}
