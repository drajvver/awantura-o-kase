﻿using System.Linq;

namespace AwanturaOKase.MainApplication
{
    class ConfigTools
    {
        public static string GetConfig(AwanturaDbEntities ctx, string key, string defaultValue = "")
        {
            Config conf = (from s in ctx.Configs where s.ConfigKey == key select s).FirstOrDefault();
            if (conf == null || conf.ConfigValue == "")
            {
                return defaultValue;
            }
            return conf.ConfigValue ?? defaultValue;
        }
    }
}
