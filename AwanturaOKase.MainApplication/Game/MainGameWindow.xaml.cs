﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace AwanturaOKase.MainApplication.Game
{
    /// <summary>
    /// Interaction logic for MainGameWindow.xaml
    /// </summary>
    public partial class MainGameWindow : Window
    {
        public DispatcherTimer gameTimer = new DispatcherTimer();
        private readonly AwanturaDbEntities ctx = new AwanturaDbEntities();
        private Questions q;
        public TextBlock selectedTeam;
        public int time;

        public MainGameWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GetTeamsInfo();
            gameTimerBlock.Text = ConfigTools.GetConfig(ctx, "RoundTime");
            time = int.Parse(gameTimerBlock.Text);
            gameTimer.Interval = TimeSpan.FromSeconds(1);
            gameTimer.Tick += gameTimer_Tick;
        }

        void gameTimer_Tick(object sender, EventArgs e)
        {
            time--;
            if(time > 0)
            {
                gameTimerBlock.Text = time.ToString();
            }
        }

        private void GetTeamsInfo()
        {
            List<Teams> teamList = ctx.Teams.ToList();
            for (int i = 0; i < teamList.Count; i++)
            {
                var teamPanel = (from s in teamInfoGrid.Children.OfType<StackPanel>() where s.Name == "team"+ (i+1) +"Info" select s).FirstOrDefault();

                var teamNameCtl = (from s in teamPanel.Children.OfType<TextBlock>() where s.Name == "team" + (i+1) + "Name" select s).FirstOrDefault();
                teamNameCtl.Text = teamList[i].TeamName;

                var teamMoneyCtl = (from s in teamPanel.Children.OfType<TextBlock>() where s.Name == "team" + (i+1) + "Money" select s).FirstOrDefault();
                teamMoneyCtl.Text = String.Format("{0} ({1})", ConfigTools.GetConfig(ctx, "StartingMoney"), 0);
            }
        }

        private void DrawCategory()
        {
            categoryDraw.Text = "";
            questionDraw.Text = "";
            hintsPanel.Children.Clear();
            Random r = new Random();
            int rr;
            for (int i = 0; i < 10; i++)
            {
                rr = r.Next(1, ctx.QuestionTypes.Count() + 1);
                Thread.Sleep(50);
                categoryDraw.Text = ctx.QuestionTypes.FirstOrDefault(x => x.QuestionTypeId == rr).QuestionTypeName;
                //categoryDraw.Text = "Informatyka";
                DoEvents();
            }
        }

        private void drawCategoryBtn_Click(object sender, RoutedEventArgs e)
        {
            DrawCategory();
        }

        public void DoEvents()
        {
            DispatcherFrame frame = new DispatcherFrame(true);
            Dispatcher.CurrentDispatcher.BeginInvoke(
                DispatcherPriority.Background,
                (SendOrPostCallback)delegate(object arg)
                {
                    var f = arg as DispatcherFrame;
                    f.Continue = false;
                },
                frame);
            Dispatcher.PushFrame(frame);
        }

        private void DrawQuestion()
        {
            var catId = ctx.QuestionTypes.FirstOrDefault(x => x.QuestionTypeName == categoryDraw.Text)?.QuestionTypeId;
            Random r = new Random();
            List<Questions> qList = ctx.Questions.Where(x => x.QuestionType == catId).ToList();
            if (qList.Count > 0)
            {
                q = qList[r.Next(0, qList.Count - 1)];

                questionDraw.Text = q.QuestionContent;
            }
        }

        private void drawQuestionBtn_Click(object sender, RoutedEventArgs e)
        {
            DrawQuestion();
        }

        private void getHintBtn_Click(object sender, RoutedEventArgs e)
        {
            Label hint = new Label {Content = "A: " + q.QuestionAnswerA};
            hintsPanel.Children.Add(hint);
            hint = new Label {Content = "B: " + q.QuestionAnswerB};
            hintsPanel.Children.Add(hint);
            hint = new Label {Content = "C: " + q.QuestionAnswerC};
            hintsPanel.Children.Add(hint);
            hint = new Label {Content = "D: " + q.QuestionAnswerD};
            hintsPanel.Children.Add(hint);
        }

        private void startGameBtn_Click(object sender, RoutedEventArgs e)
        {
            //gameTimer.Start();
        }
    }
}
