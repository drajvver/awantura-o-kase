﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using AwanturaOKase.MainApplication.Helpers;

namespace AwanturaOKase.MainApplication.Game
{
    /// <summary>
    /// Interaction logic for AdminGameWindow.xaml
    /// </summary>
    public partial class AdminGameWindow : Window
    {
        public AdminGameWindow()
        {
            InitializeComponent();
        }
        private readonly DispatcherTimer gameTimer = new DispatcherTimer();
        private readonly AwanturaDbEntities ctx = new AwanturaDbEntities();
        private readonly MainGameWindowHelpers _helpers = new MainGameWindowHelpers();
        private Questions q;
        private MainGameWindow mgw = new MainGameWindow();
        int time;
        private MediaPlayer mp;
        private readonly List<Questions> _usedQuestions = new List<Questions>(); 
        private Dictionary<QuestionTypes, int> onceDraws = new Dictionary<QuestionTypes, int>();
        private List<QuestionTypes> qTypes;
        private TextBlock selectedTeam;



        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GetTeamsInfo();
            gameTimerBlock.Text = ConfigTools.GetConfig(ctx, "RoundTime");
            time = int.Parse(gameTimerBlock.Text);
            gameTimer.Interval = TimeSpan.FromSeconds(1);
            gameTimer.Tick += gameTimer_Tick;
            mgw.Closed += mgw_Closed;
            mp = new MediaPlayer();
            qTypes = ctx.QuestionTypes.Where(t => t.Questions.Any()).ToList();
        }

        void mgw_Closed(object sender, EventArgs e)
        {
            startGameBtn.IsEnabled = true;
        }

        void gameTimer_Tick(object sender, EventArgs e)
        {
            time--;
            if(time > 0)
            {
                gameTimerBlock.Text = time.ToString();
            }
        }

        private void GetTeamsInfo()
        {
            var teamList = ctx.Teams.ToList();
            for (var i = 0; i < teamList.Count; i++)
            {
                var teamPanel = (from s in teamInfoGrid.Children.OfType<StackPanel>() where s.Name == "team"+ (i+1) +"Info" select s).FirstOrDefault();

                var teamNameCtl = (from s in teamPanel.Children.OfType<TextBlock>() where s.Name == "team" + (i+1) + "Name" select s).FirstOrDefault();
                teamNameCtl.Text = teamList[i].TeamName;

                var teamMoneyCtl = (from s in teamPanel.Children.OfType<TextBlock>() where s.Name == "team" + (i+1) + "Money" select s).FirstOrDefault();
                teamMoneyCtl.Text = String.Format("{0} ({1})", ConfigTools.GetConfig(ctx, "StartingMoney"), 0);
            }
        }

        private void DrawCategory()
        {
            mgw.RightAnswerBlock.Visibility = Visibility.Hidden;
            categoryDraw.Text = "";
            questionDraw.Text = "";
            mgw.categoryDraw.Text = "";
            mgw.questionDraw.Text = "";
            mgw.hintsPanel.Children.Clear();
            mgw.QuestionImage.Source = null;
            QuestionImage.Source = null;
            //mgw.hintsPanel2.Children.Clear();
            hintsPanel.Children.Clear();
            var u = new Uri(@"Sounds/losowanie.mp3", UriKind.RelativeOrAbsolute);
            
            mp.Open(u);
            mp.Play();
            var r = new Random();
            var qt = new QuestionTypes();
            bool? onlyOnce = false;
            if (!qTypes.Any() || onceDraws.Count == ctx.QuestionTypes.Count() || qt == null)
            {
                MessageBox.Show("KONIEC GRY, KONIEC PYTAŃ!");
                return;
            }
            for (var i = 0; i < 115; i++)
            {
                var s = 0.0;
                if (i <= 50)
                    s = 1;
                else if (i > 50 && i < 101)
                    s = 2;
                else
                    s = (double) i/20;
                qt = qTypes.ElementAtOrDefault(r.Next(qTypes.Count));
                if (qt == null)
                {
                    MessageBox.Show("KONIEC GRY, KONIEC PYTAŃ!");
                    return;
                }
                //qt = ctx.QuestionTypes.FirstOrDefault();
                if (!qt.Questions.Any())
                {
                    qTypes.Remove(qt);
                    continue;
                }
                onlyOnce = qt.QuestionDrawOnce;
                categoryDraw.Text = qt.QuestionTypeName;
                mgw.categoryDraw.Text = qt.QuestionTypeName;
                //categoryDraw.Text = "Sejf p. Basi";
                DoEvents();
                Thread.Sleep(TimeSpan.FromMilliseconds(25*s));
            }
            if (!qTypes.Any() || onceDraws.Count == ctx.QuestionTypes.Count() || qt == null)
            {
                MessageBox.Show("KONIEC GRY, KONIEC PYTAŃ!");
                return;
            }
            if (onlyOnce == true && onceDraws.ContainsKey(qt))
            {
                qTypes.Remove(qt);
                DrawCategory();
            }
            if (onceDraws.ContainsKey(qt))
            {
                DrawCategory();
            }
            if (onlyOnce == true)
            {
                qTypes.Remove(qt);
                onceDraws.Add(qt, 1);
            }
            //mgw.CategoryImage.Source = qt.QuestionImgImageSource;
                
        }

        private void drawCategoryBtn_Click(object sender, RoutedEventArgs e)
        {
            mgw.RightAnswerBlock.Text = "";
            DrawCategory();
            drawQuestionBtn.IsEnabled = true;
        }

        public void DoEvents()
        {
            var frame = new DispatcherFrame(true);
            Dispatcher.CurrentDispatcher.BeginInvoke(
                DispatcherPriority.Background,
                (SendOrPostCallback)delegate(object arg)
                {
                    var f = arg as DispatcherFrame;
                    f.Continue = false;
                },
                frame);
            Dispatcher.PushFrame(frame);
        }

        private void DrawQuestion()
        {
            //categoryDraw.Text = "";
            //questionDraw.Text = "";
            //mgw.categoryDraw.Text = "";
            mgw.questionDraw.Text = "";
            mgw.hintsPanel.Children.Clear();
            //mgw.hintsPanel2.Children.Clear();
            mgw.QuestionImage.Source = null;
            QuestionImage.Source = null;
            hintsPanel.Children.Clear();
            var catId = ctx.QuestionTypes.FirstOrDefault(x => x.QuestionTypeName == categoryDraw.Text).QuestionTypeId;
            var r = new Random();
            var qList = ctx.Questions.Where(x => x.QuestionType == catId).ToList();
            if (qList.Count > 0)
            {
                q = qList[r.Next(0, qList.Count)];
                
                if ((onceDraws.ContainsKey(q.QuestionTypes) && onceDraws[q.QuestionTypes] > 1) || _usedQuestions.Count(x => x.QuestionType == catId) == qList.Count)
                {
                    if (
                        MessageBox.Show("Koniec pytań, czy chcesz wyslosować inną kategorię?", "Koniec pytań!", MessageBoxButton.YesNo) ==
                        MessageBoxResult.Yes)
                    {
                        qTypes.Remove(q.QuestionTypes);
                        DrawCategory();
                    }
                    else
                    {
                        return;
                    }
                    //usedQuestions.RemoveAll(x => x.QuestionType == catId);
                    //DrawQuestion();
                }
                else if (_usedQuestions.Contains(q))
                    DrawQuestion();
                else
                {
                    questionDraw.Text = q.QuestionContent;
                    if (q.QuestionImgImageSource != null)
                    {
                        QuestionImage.Visibility = Visibility.Visible;
                        mgw.QuestionImage.Visibility = Visibility.Visible;
                        //mgw.MainSeparator.Visibility = Visibility.Visible;
                        QuestionImage.Source = q.QuestionImgImageSource;
                        mgw.QuestionImage.Source = q.QuestionImgImageSource;
                    }
                    else
                    {
                        QuestionImage.Visibility = Visibility.Collapsed;
                        mgw.QuestionImage.Visibility = Visibility.Collapsed;
                        //mgw.MainSeparator.Visibility = Visibility.Collapsed;
                    }
                    mgw.questionDraw.Text = q.QuestionContent;
                    RightAnswerBlock.Text = q.QuestionRightAnswer;
                    _usedQuestions.Add(q);
                    qTypes.FirstOrDefault(t => t.QuestionTypeId == catId)?.Questions?.Remove(q);
                }
  
            }
        }

        private void drawQuestionBtn_Click(object sender, RoutedEventArgs e)
        {
            DrawQuestion();

            if (!mgw.gameTimer.IsEnabled)
            {
                mgw.gameTimerBlock.Text = ConfigTools.GetConfig(ctx, "RoundTime");
                gameTimerBlock.Text = ConfigTools.GetConfig(ctx, "RoundTime");
                mgw.gameTimer.Start();
                gameTimer.Start();
            }
            else
            {
                mgw.gameTimerBlock.Text = ConfigTools.GetConfig(ctx, "RoundTime");
                mgw.gameTimer.Stop();
                gameTimerBlock.Text = ConfigTools.GetConfig(ctx, "RoundTime");
                time = int.Parse(gameTimerBlock.Text);
                mgw.time = int.Parse(gameTimerBlock.Text);
                gameTimer.Stop();
            }
        }

        private void getHintBtn_Click(object sender, RoutedEventArgs e)
        {
            hintsPanel.Children.Clear();
            mgw.hintsPanel.Children.Clear();
            //mgw.hintsPanel2.Children.Clear();
            if (q == null || q.QuestionImage != null) return;
            var hint = new Label {Content = "A: " + q.QuestionAnswerA, Margin = new Thickness(5), FontSize = 20};
            hintsPanel.Children.Add(hint);
            hint = new Label {Content = "A: " + q.QuestionAnswerA, Margin = new Thickness(5), FontSize = 30};
            mgw.hintsPanel.Children.Add(hint);
            hint = new Label {Content = "B: " + q.QuestionAnswerB, Margin = new Thickness(5), FontSize = 20};
            hintsPanel.Children.Add(hint);
            hint = new Label {Content = "B: " + q.QuestionAnswerB, Margin = new Thickness(5), FontSize = 30};
            mgw.hintsPanel.Children.Add(hint);
            hint = new Label {Content = "C: " + q.QuestionAnswerC, Margin = new Thickness(5), FontSize = 20};
            hintsPanel.Children.Add(hint);
            hint = new Label {Content = "C: " + q.QuestionAnswerC, Margin = new Thickness(5), FontSize = 30};
            mgw.hintsPanel.Children.Add(hint);
            hint = new Label {Content = "D: " + q.QuestionAnswerD, Margin = new Thickness(5), FontSize = 20};
            hintsPanel.Children.Add(hint);
            hint = new Label {Content = "D: " + q.QuestionAnswerD, Margin = new Thickness(5), FontSize = 30};
            mgw.hintsPanel.Children.Add(hint);

            mgw.hintsPanel.Visibility = Visibility.Visible;
        }

        private void startGameBtn_Click(object sender, RoutedEventArgs e)
        {
            mgw = new MainGameWindow();
            mgw.Show();
            startGameBtn.IsEnabled = false;
            //gameTimer.Start();
            //mgw.gameTimer.Start();

            if (mgw.ActualHeight < 1079 || mgw.ActualWidth < 1919)
            {
                foreach (var child in _helpers.FindLogicalChildren<TextBlock>(mgw))
                {
                    child.FontSize /= 2;
                }

                foreach (var child in _helpers.FindLogicalChildren<Label>(mgw))
                {
                    child.FontSize /= 2;
                }
            }
        }

        int pool = 0;
        
        #region Kupa niepotrzebnych zmiennych ale nie umiem inaczej
        private int minus_click_t1 = 0;
        private int minus_click_t2 = 0;
        private int minus_click_t3 = 0;
        private int minus_click_t4 = 0;
        private int minus_click_t5 = 0;
        private int minus_click_t6 = 0;
        private int minus_click_t7 = 0;
        private int minus_click_t8 = 0;
        private int minus_click_t9 = 0;
        private int minus_click_t10 = 0;
        private int minus_click_t11 = 0;
        private int minus_click_t12 = 0;

        private int plus_click_t1 = 0;
        private int plus_click_t2 = 0;
        private int plus_click_t3 = 0;
        private int plus_click_t4 = 0;
        private int plus_click_t5 = 0;
        private int plus_click_t6 = 0;
        private int plus_click_t7 = 0;
        private int plus_click_t8 = 0;
        private int plus_click_t9 = 0;
        private int plus_click_t10 = 0;
        private int plus_click_t11 = 0;
        private int plus_click_t12 = 0;
#endregion
        private void minus100Btn_Click(object sender, RoutedEventArgs e)
        {
            var money = 0;
            var b = sender as Button;
            switch(b.Tag.ToString())
            {
                case "1":
                    money = int.Parse(team1Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        var money2 = int.Parse(team1Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        minus_click_t1++;
                        team1Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                        mgw.team1Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                    }
                    break;
                case "2":
                    money = int.Parse(team2Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        var money2 = int.Parse(team2Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        minus_click_t2++;
                        team2Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                        mgw.team2Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                    }
                    break;
                case "3":
                    money = int.Parse(team3Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        var money2 = int.Parse(team3Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        minus_click_t3++;
                        team3Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                        mgw.team3Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                    }
                    break;
                case "4":
                    money = int.Parse(team4Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        var money2 = int.Parse(team4Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        minus_click_t4++;
                        team4Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                        mgw.team4Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                    }
                    break;
                case "5":
                    money = int.Parse(team5Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        var money2 = int.Parse(team5Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        minus_click_t5++;
                        team5Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                        mgw.team5Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                    }
                    break;
                case "6":
                    money = int.Parse(team6Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        var money2 = int.Parse(team6Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        minus_click_t6++;
                        team6Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                        mgw.team6Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                    }
                    break;
                case "7":
                    money = int.Parse(team7Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        var money2 = int.Parse(team7Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        minus_click_t7++;
                        team7Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                        mgw.team7Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                    }
                    break;
                case "8":
                    money = int.Parse(team8Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        var money2 = int.Parse(team8Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        minus_click_t8++;
                        team8Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                        mgw.team8Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                    }
                    break;
                case "9":
                    money = int.Parse(team9Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        var money2 = int.Parse(team9Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        minus_click_t9++;
                        team9Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                        mgw.team9Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                    }
                    break;
                case "10":
                    money = int.Parse(team10Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        var money2 = int.Parse(team10Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        minus_click_t10++;
                        team10Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                        mgw.team10Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                    }
                    break;
                case "11":
                    money = int.Parse(team11Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        var money2 = int.Parse(team11Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        minus_click_t11++;
                        team11Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                        mgw.team11Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                    }
                    break;
                case "12":
                    money = int.Parse(team12Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        var money2 = int.Parse(team12Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        minus_click_t12++;
                        team12Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                        mgw.team12Money.Text = String.Format("{0} ({1})", (money - 100).ToString(), (money2 + 100).ToString());
                    }
                    break;
            }
            if(money > 0)
                pool = pool + 100;
            mgw.moneyPoolBlock.Text = String.Format("W puli mamy: {0} zł.", pool);
        }

        private void hideStatsOnMainScreenBtn_Click(object sender, RoutedEventArgs e)
        {
            if (mgw.teamInfoGrid.Visibility == Visibility.Hidden)
                mgw.teamInfoGrid.Visibility = Visibility.Visible;
            else
                mgw.teamInfoGrid.Visibility = Visibility.Hidden;
        }

        private void auctionHint_Click(object sender, RoutedEventArgs e)
        {
            if (selectedTeam != null && (int.Parse(selectedTeam.Text.Split(' ')[0]) > 0))
            {
                var a = selectedTeam.Text.Split(' ');
                var money = int.Parse(selectedTeam.Text.Split(' ')[0]);
                selectedTeam.Text = String.Format("{0} {1}", (money - 100), a[1]);
                mgw.selectedTeam.Text = String.Format("{0} {1}", (money - 100), a[1]);
            }
            else if (selectedTeam == null)
            {
                MessageBox.Show("Wybierz drużynę!");
            }
            else
            {
                MessageBox.Show("Wybrana drużyna nie ma kasy!");
            }
        }

        private void team1Name_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if(selectedTeam != null)
                selectedTeam.Background = Brushes.Transparent;

            selectedTeam = sender as TextBlock;
            selectedTeam.Background = Brushes.DarkGray;
            var SPname = selectedTeam.Name.Replace("Money", "Info").Replace("Name", "Info");
            var TBname = selectedTeam.Name.Replace("Money", "Name");
            var sp = (from s in teamInfoGrid.Children.OfType<StackPanel>() where s.Name == SPname select s).FirstOrDefault();

            var tb = (from s in sp.Children.OfType<TextBlock>() where s.Name == TBname select s).FirstOrDefault();

            var sp2 = (from s in mgw.teamInfoGrid.Children.OfType<StackPanel>() where s.Name == SPname select s).FirstOrDefault();

            var tb2 = (from s in sp2.Children.OfType<TextBlock>() where s.Name == selectedTeam.Name select s).FirstOrDefault();
            mgw.selectedTeam = tb2;
        }

        private void ClearAuctionMoney()
        {
            minus_click_t1 = 0;
            minus_click_t2 = 0;
            minus_click_t3 = 0;
            minus_click_t4 = 0;
            minus_click_t5 = 0;
            minus_click_t6 = 0;
            minus_click_t7 = 0;
            minus_click_t8 = 0;
            minus_click_t9 = 0;
            minus_click_t10 = 0;
            minus_click_t11 = 0;
            minus_click_t12 = 0;

            plus_click_t1 = 0;
            plus_click_t2 = 0;
            plus_click_t3 = 0;
            plus_click_t4 = 0;
            plus_click_t5 = 0;
            plus_click_t6 = 0;
            plus_click_t7 = 0;
            plus_click_t8 = 0;
            plus_click_t9 = 0;
            plus_click_t10 = 0;
            plus_click_t11 = 0;
            plus_click_t12 = 0;

            for (var i = 1; i < ctx.Teams.Count()+1; i++)
            {
                var teamBox = this.FindName(String.Format("team{0}Money", i)) as TextBlock;
                var s = teamBox.Text;
                teamBox.Text = String.Format("{0} ({1})", s.Split(' ')[0], 0);

                var teamBox2 = mgw.FindName(String.Format("team{0}Money", i)) as TextBlock;
                var s2 = teamBox.Text;
                teamBox2.Text = String.Format("{0} ({1})", s2.Split(' ')[0], 0);
            }
        }

        private void goodAnswer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (selectedTeam == null)
                    return;
                mp.Open(new Uri(@"Sounds/goodAnswer.mp3", UriKind.RelativeOrAbsolute));
                mp.Play();
                ClearAuctionMoney();
                var money = int.Parse(selectedTeam.Text.Split(' ')[0]);
                selectedTeam.Text = String.Format("{0} ({1})", (money + pool).ToString(), 0);
                selectedTeam.Background = Brushes.Transparent;
                var SPname = selectedTeam.Name.Replace("Money", "Info");
                var sp =
                    (from s in mgw.teamInfoGrid.Children.OfType<StackPanel>() where s.Name == SPname select s)
                        .FirstOrDefault();

                var tb =
                    (from s in sp.Children.OfType<TextBlock>() where s.Name == selectedTeam.Name select s)
                        .FirstOrDefault();
                tb.Text = String.Format("{0} ({1})", (money + pool).ToString(), 0);
                pool = 0;
                //mgw.hintsPanel.Children.Clear();
                //mgw.hintsPanel2.Children.Clear();
                mgw.moneyPoolBlock.Text = "";
                //mgw.questionDraw.Text = "";
                //mgw.categoryDraw.Text = "";
                //mgw.QuestionImage.Source = null;
                if (q != null && q.QuestionRightAnswer != null)
                    mgw.RightAnswerBlock.Text = q.QuestionRightAnswer;
                gameTimer.Stop();
                gameTimerBlock.Text = ConfigTools.GetConfig(ctx, "RoundTime");
                mgw.gameTimer.Stop();
                mgw.gameTimerBlock.Text = ConfigTools.GetConfig(ctx, "RoundTime");

                if (q?.QuestionImage != null)
                    mgw.RightAnswerBlock.Visibility = Visibility.Visible;

                else if (q?.QuestionImage == null && mgw.RightAnswerBlock.Visibility != Visibility.Visible)
                    mgw.RightAnswerBlock.Visibility = Visibility.Visible;
                
                var lbl = mgw.hintsPanel.Children.OfType<Label>();
                var toChange = lbl.FirstOrDefault(t => t.HasContent && (t.Content as string)?.Contains(q.QuestionRightAnswer) == true);
                _helpers.ChangeTextColor(toChange);

            }
            catch { }
        }

        private void wrongAnswer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (selectedTeam == null)
                    return;
                mp.Open(new Uri(@"Sounds/wrongAnswer.mp3", UriKind.RelativeOrAbsolute));
                mp.Play();
                ClearAuctionMoney();
                selectedTeam.Background = Brushes.Transparent;
                //mgw.hintsPanel.Children.Clear();
                //mgw.hintsPanel2.Children.Clear();
                //mgw.questionDraw.Text = "";
                //mgw.categoryDraw.Text = "";
                //mgw.QuestionImage.Source = null;
                if (q != null && q.QuestionRightAnswer != null)
                    mgw.RightAnswerBlock.Text = q.QuestionRightAnswer;
                gameTimer.Stop();
                gameTimerBlock.Text = ConfigTools.GetConfig(ctx, "RoundTime");
                mgw.gameTimer.Stop();
                mgw.gameTimerBlock.Text = ConfigTools.GetConfig(ctx, "RoundTime");

                if (q?.QuestionImage != null)
                    mgw.hintsPanel.Visibility = Visibility.Visible;

                if (q?.QuestionImage != null && mgw.hintsPanel.Visibility != Visibility.Visible)
                    mgw.RightAnswerBlock.Visibility = Visibility.Visible;

                var lbl = mgw.hintsPanel.Children.OfType<Label>();
                var toChange = lbl.FirstOrDefault(t => t.HasContent && (t.Content as string)?.Contains(q.QuestionRightAnswer) == true);
                _helpers.ChangeTextColor(toChange);
            }
            catch { }
        }

        private void timerStart_Click(object sender, RoutedEventArgs e)
        {
            //mp.Open(new Uri(@"Sounds/timerStart.mp3", UriKind.RelativeOrAbsolute));
            if(!mgw.gameTimer.IsEnabled)
            {
                mgw.gameTimer.Start();
                gameTimer.Start();
            }
            else
            {
                mgw.gameTimerBlock.Text = ConfigTools.GetConfig(ctx, "RoundTime");
                mgw.gameTimer.Stop();
                gameTimerBlock.Text = ConfigTools.GetConfig(ctx, "RoundTime");
                time = int.Parse(gameTimerBlock.Text);
                mgw.time = int.Parse(gameTimerBlock.Text);
                gameTimer.Stop();
            }
        }

        private void plus100Btn_Click(object sender, RoutedEventArgs e)
        {
            var money = 0;
            if(pool == 0)
                return;
            var b = sender as Button;
            switch (b.Tag.ToString())
            {
                case "+1":
                    money = int.Parse(team1Money.Text.Split(' ')[0]);
                    if (money >= 0)
                    {
                        var money2 = int.Parse(team1Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        plus_click_t1--;
                        team1Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                        mgw.team1Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                    }
                    break;
                case "+2":
                    money = int.Parse(team2Money.Text.Split(' ')[0]);
                    if (money >= 0)
                    {
                        var money2 = int.Parse(team2Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        plus_click_t2--;
                        team2Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                        mgw.team2Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                    }
                    break;
                case "+3":
                    money = int.Parse(team3Money.Text.Split(' ')[0]);
                    if (money >= 0)
                    {
                        var money2 = int.Parse(team3Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        plus_click_t3--;
                        team3Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                        mgw.team3Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                    }
                    break;
                case "+4":
                    money = int.Parse(team4Money.Text.Split(' ')[0]);
                    if (money >= 0)
                    {
                        var money2 = int.Parse(team4Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        plus_click_t4--;
                        team4Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                        mgw.team4Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                    }
                    break;
                case "+5":
                    money = int.Parse(team5Money.Text.Split(' ')[0]);
                    if (money >= 0)
                    {
                        var money2 = int.Parse(team5Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        plus_click_t5--;
                        team5Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                        mgw.team5Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                    }
                    break;
                case "+6":
                    money = int.Parse(team6Money.Text.Split(' ')[0]);
                    if (money >= 0)
                    {
                        var money2 = int.Parse(team6Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        plus_click_t6--;
                        team6Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                        mgw.team6Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                    }
                    break;
                case "+7":
                    money = int.Parse(team7Money.Text.Split(' ')[0]);
                    if (money >= 0)
                    {
                        var money2 = int.Parse(team7Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        plus_click_t7--;
                        team7Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                        mgw.team7Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                    }
                    break;
                case "+8":
                    money = int.Parse(team8Money.Text.Split(' ')[0]);
                    if (money >= 0)
                    {
                        var money2 = int.Parse(team8Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        plus_click_t8--;
                        team8Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                        mgw.team8Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                    }
                    break;
                case "+9":
                    money = int.Parse(team9Money.Text.Split(' ')[0]);
                    if (money >= 0)
                    {
                        var money2 = int.Parse(team9Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        plus_click_t9--;
                        team9Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                        mgw.team9Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                    }
                    break;
                case "+10":
                    money = int.Parse(team10Money.Text.Split(' ')[0]);
                    if (money >= 0)
                    {
                        var money2 = int.Parse(team10Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        plus_click_t10--;
                        team10Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                        mgw.team10Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                    }
                    break;
                case "+11":
                    money = int.Parse(team11Money.Text.Split(' ')[0]);
                    if (money >= 0)
                    {
                        var money2 = int.Parse(team11Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        plus_click_t11--;
                        team11Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                        mgw.team11Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                    }
                    break;
                case "+12":
                    money = int.Parse(team12Money.Text.Split(' ')[0]);
                    if (money >= 0)
                    {
                        var money2 = int.Parse(team12Money.Text.Split(' ')[1].Replace("(", "").Replace(")", ""));
                        plus_click_t12--;
                        team12Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                        mgw.team12Money.Text = String.Format("{0} ({1})", (money + 100).ToString(), (money2 - 100).ToString());
                    }
                    break;
            }
            pool = pool - 100;
            mgw.moneyPoolBlock.Text = String.Format("W puli mamy: {0} zł.", pool);
        }


        private int cl = 0;
        private void StartMusic_OnClick(object sender, RoutedEventArgs e)
        {
            cl++;
            mp.Open(new Uri(@"Sounds/intor.mp3", UriKind.RelativeOrAbsolute));
            mp.Play();

            if (cl%2 == 0)
            {
                mp.Stop();
                cl = 0;
            }
        }

        private void StartNarada_OnClick(object sender, RoutedEventArgs e)
        {
            cl++;
            mp.Open(new Uri(@"Sounds/narada22s.mp3", UriKind.RelativeOrAbsolute));
            mp.Play();

            if (cl % 2 == 0)
            {
                mp.Stop();
                cl = 0;
            }
        }

        private void ClearPool_OnClick(object sender, RoutedEventArgs e)
        {
            pool = 0;
            mgw.moneyPoolBlock.Text = String.Format("W puli mamy: {0} zł.", pool);
            ClearAuctionMoney();
        }

        private void vaBanqueBtn_Click(object sender, RoutedEventArgs e)
        {
            var money = 0;
            var b = sender as Button;
            switch (b.Tag.ToString())
            {
                case "VB1":
                    money = int.Parse(team1Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        team1Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                        mgw.team1Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                    }
                    break;
                case "VB2":
                    money = int.Parse(team2Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        team2Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                        mgw.team2Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                    }
                    break;
                case "VB3":
                    money = int.Parse(team3Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        team3Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                        mgw.team3Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                    }
                    break;
                case "VB4":
                    money = int.Parse(team4Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        team4Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                        mgw.team4Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                    }
                    break;
                case "VB5":
                    money = int.Parse(team5Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        team5Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                        mgw.team5Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                    }
                    break;
                case "VB6":
                    money = int.Parse(team6Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        team6Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                        mgw.team6Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                    }
                    break;
                case "VB7":
                    money = int.Parse(team7Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        team7Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                        mgw.team7Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                    }
                    break;
                case "VB8":
                    money = int.Parse(team8Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        team8Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                        mgw.team8Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                    }
                    break;
                case "VB9":
                    money = int.Parse(team9Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        team9Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                        mgw.team9Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                    }
                    break;
                case "VB10":
                    money = int.Parse(team10Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        team10Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                        mgw.team10Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                    }
                    break;
                case "VB11":
                    money = int.Parse(team11Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        team11Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                        mgw.team11Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                    }
                    break;
                case "VB12":
                    money = int.Parse(team12Money.Text.Split(' ')[0]);
                    if (money > 0)
                    {
                        team12Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                        mgw.team12Money.Text = String.Format("{0} ({1})", 0, (money).ToString());
                    }
                    break;
            }
            pool += money;
            mgw.moneyPoolBlock.Text = String.Format("W puli mamy: {0} zł.", pool);
        }
    }
}
