﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace AwanturaOKase.MainApplication.Helpers
{
    public class MainGameWindowHelpers
    {
        public void ChangeTextColor(Label obj)
        {
            if(obj != null)
                obj.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromRgb(20, 255, 100));
        }

        public IEnumerable<T> FindLogicalChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                foreach (var rawChild in LogicalTreeHelper.GetChildren(depObj))
                {
                    if (rawChild is DependencyObject)
                    {
                        var child = (DependencyObject)rawChild;
                        if (child is T)
                        {
                            yield return (T)child;
                        }

                        foreach (var childOfChild in FindLogicalChildren<T>(child))
                        {
                            yield return childOfChild;
                        }
                    }
                }
            }
        }
    }
}